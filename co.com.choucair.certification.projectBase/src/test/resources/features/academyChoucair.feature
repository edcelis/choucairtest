#Autor: Elian Celis
@stories
Feature: Academy Choucair
  As a user, I want to learn how to automate in sreamplay at the Choucair Academy with the automation course
  @scenario1
  Scenario:  Search for a automation course
  Given than David wants to learn automation at the Choucair Academy
    | strUser | strPassword |
    | 1000156054 | Choucair2021* |
  When he search for the course on the Choucair Academy platform
    | strCourse |
    | Metodología Bancolombia |
  Then he finds the course called resources
    | strCourse |
    | Metodología Bancolombia |