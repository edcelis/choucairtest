package co.com.choucair.certification.projectBase.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage extends PageObject {
    public static final Target BUTTON_CC = Target.the("button for select the choucar certification").located(By.xpath("//div[@id='certificaciones']//strong"));
    public static final Target NAME_COURSE = Target.the("box for search the course").located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("button for search the course").located(By.xpath("//button[@class='btn btn-secondary']"));
    public static final Target SELECT_COURSE = Target.the("Button for search the course").located(By.xpath("//a[@href='https://operacion.choucairtesting.com/academy/course/view.php?id=489']"));
}
